# Findex-Ferry

[Repo](https://gitlab.com/marcell93/findex-ferry) 

[Demo](https://marcell93.gitlab.io/findex-ferry/) 

## Getting started

Angular test proyect.

With each push in the main branch, the code is deployed to the demo url.

## Run locally

```
npm i
npm start
```

## Unit test

```
npm run test
```