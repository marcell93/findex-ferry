import { VehicleSize, VehicleSummary } from "./ivehicle.provider";

export class Ferry {
    category: VehicleSize;
    holdedVehicles: VehicleSummary[] = [];
    maximumVehicleCapacity: number;

    constructor(category: VehicleSize) {
        this.category = category;
        if (this.category === VehicleSize.small) {
            this.maximumVehicleCapacity = 8;
        } else {
            this.maximumVehicleCapacity = 6;
        }
    }

    addVehicle(vehicle: VehicleSummary): boolean {
        if (vehicle) {
            if (this.holdedVehicles?.length >= this.maximumVehicleCapacity) {
                throw new Error(`The ferry is full. A ${this.category} ferry can hold ${this.maximumVehicleCapacity} ${this.category} vehicles`);
            }
            this.holdedVehicles.push(vehicle);
            return true;
        } else {
            throw new Error('Enter a valid vehicle');
        }
    }

    getLastVehicle() {
        return this.holdedVehicles[this.holdedVehicles.length - 1];
    }

    getFreeSpaces() {
        return this.maximumVehicleCapacity - this.holdedVehicles.length;
    }
}