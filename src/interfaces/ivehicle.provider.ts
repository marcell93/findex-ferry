import { InjectionToken } from "@angular/core";

export const VEHICLE_PROVIDER = new InjectionToken("IVehicleProvider");

export interface IVehicleProvider {
  GetVehicle(): VehicleSummary;
}

export class VehicleSummary {
  type: VehicleType;
  category: VehicleSize;
  cost: number;
  added: Date;
}

export enum VehicleSize {
  small = 'small',
  large = 'large'
}
export enum VehicleType {
  car = 'car',
  van = 'van',
  truck = 'truck',
  bus = 'bus'
}
