import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FerryTerminalComponent } from "./ferryterminal/ferry.terminal.component";
import { AppComponent } from "./app.component";
import { HardcodedVehicleService } from "../services/hardcoded.vehicle.service";
import { VEHICLE_PROVIDER } from "src/interfaces/ivehicle.provider";
import { FerryService } from "src/services/ferry.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [AppComponent, FerryTerminalComponent],
  imports: [BrowserModule, BrowserAnimationsModule, ToastrModule.forRoot(), MatButtonModule, MatTableModule,],
  providers: [FerryService, { provide: VEHICLE_PROVIDER, useClass: HardcodedVehicleService }],
  bootstrap: [AppComponent]
})
export class AppModule { }
