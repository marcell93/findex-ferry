import { DebugElement } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { VEHICLE_PROVIDER } from 'src/interfaces/ivehicle.provider';
import { FerryService } from 'src/services/ferry.service';
import { HardcodedVehicleService } from 'src/services/hardcoded.vehicle.service';
import { FerryTerminalComponent } from './ferry.terminal.component';
describe('FerryTerminalComponent', () => {
    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                FerryTerminalComponent
            ],
            providers: [
                FerryService, { provide: VEHICLE_PROVIDER, useClass: HardcodedVehicleService }
            ],
            imports: [BrowserAnimationsModule, ToastrModule.forRoot(), MatTableModule]
        }).compileComponents();
    }));

    it('should create the app', waitForAsync(() => {
        const fixture = TestBed.createComponent(FerryTerminalComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it(`should add an item to the table`, () => {
        const fixture = TestBed.createComponent(FerryTerminalComponent);
        let trTable = fixture.debugElement.nativeElement.querySelector('tr');
        expect(trTable).toBeNull();
        const button: DebugElement = fixture.debugElement.query(By.css('button[name=get-vehicle]'));
        button.triggerEventHandler('click',null);
        fixture.detectChanges();
        trTable = fixture.debugElement.nativeElement.querySelector('tr');
        expect(trTable).toBeTruthy();
    });

    it(`should delete the table`, () => {
        const fixture = TestBed.createComponent(FerryTerminalComponent);
        let trTable = fixture.debugElement.nativeElement.querySelector('tr');
        expect(trTable).toBeNull();
        const primaryButton: DebugElement = fixture.debugElement.query(By.css('button[name=get-vehicle]'));
        primaryButton.triggerEventHandler('click',null);
        fixture.detectChanges();
        trTable = fixture.debugElement.nativeElement.querySelector('tr');
        expect(trTable).toBeTruthy();
        const emptyFerriesButton: DebugElement = fixture.debugElement.query(By.css('button[name=empty-ferries]'));
        emptyFerriesButton.triggerEventHandler('click',null);
        fixture.detectChanges();
        trTable = fixture.debugElement.nativeElement.querySelector('tr');
        expect(trTable).toBeNull();
    });
});