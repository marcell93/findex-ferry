import { Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import {
  IVehicleProvider,
  VehicleSummary,
  VEHICLE_PROVIDER
} from "src/interfaces/ivehicle.provider";
import { FerryService } from "src/services/ferry.service";
import { merge } from 'rxjs';
import { capitalizeFirstLetter } from "../utils";

@Component({
  selector: "ferry-terminal",
  templateUrl: "./ferry.terminal.component.html",
  styleUrls: ["./ferry.terminal.component.css"]
})
export class FerryTerminalComponent implements OnInit, OnDestroy {
  private currentVehicle: VehicleSummary;
  private subscriptions: Subscription[] = []

  dataSource: VehicleSummary[] = [];
  displayedColumns: string[] = ['type', 'size', 'cost'];
  displayedColumns2: string[] = this.displayedColumns.map(x => `f2_${x}`);
  displayedColumns3: string[] = this.displayedColumns.map(x => `f3_${x}`);
  terminalEarns = 0;
  workerEarns = 0;

  constructor(
    @Inject(VEHICLE_PROVIDER) private _vehicleProvider: IVehicleProvider,
    private _ferryService: FerryService,
    private _toastrService: ToastrService,
  ) {

  }

  ngOnInit(): void {
    const suscription = merge(this._ferryService.getSmallFerry(), this._ferryService.getLargeFerry()).subscribe((ferry) => {
      const lastVehicle = ferry.getLastVehicle();
      if (lastVehicle) {
        this._toastrService.success(`${capitalizeFirstLetter(lastVehicle.type)} added to ${lastVehicle.category} ferry. There are ${ferry.getFreeSpaces()} free places`);
        this.dataSource = this.dataSource.concat(lastVehicle);
        this.terminalEarns += lastVehicle.cost;
        this.workerEarns = this.terminalEarns / 10;
        console.log(this.dataSource)
      }
    });
    this.subscriptions.push(suscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  public getVehicle() {
    try {
      this.currentVehicle = this._vehicleProvider.GetVehicle();
      this._ferryService.addVehicle(this.currentVehicle);
      if (this.dataSource?.length === 1) {
        setTimeout(() => {
          window.scrollTo(0, document.body.scrollHeight);
        }, 0);
      }
    } catch (e) {
      this._toastrService.error(e);
    }
  }

  public emptyFerries() {
    this._ferryService.emptyFerries();
    this._toastrService.success('The ferries are empty');
    this.dataSource = [];
    this.terminalEarns = 0;
    this.workerEarns = 0;
  }
}
