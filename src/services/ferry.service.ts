import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Ferry } from "src/interfaces/ferry.model";
import { VehicleSize, VehicleSummary } from "src/interfaces/ivehicle.provider";

@Injectable({
    providedIn: 'root',
})
export class FerryService {
    private _smallFerry$: BehaviorSubject<Ferry> = new BehaviorSubject<Ferry>(new Ferry(VehicleSize.small));
    private _largeFerry$: BehaviorSubject<Ferry> = new BehaviorSubject<Ferry>(new Ferry(VehicleSize.large));

    constructor(

    ) { }

    public getSmallFerry(): Observable<Ferry> {
        return this._smallFerry$.asObservable();
    }

    public getLargeFerry(): Observable<Ferry> {
        return this._largeFerry$.asObservable();
    }

    emptyFerries() {
        this._smallFerry$.next(new Ferry(VehicleSize.small));
        this._largeFerry$.next(new Ferry(VehicleSize.large));
    }

    addVehicle(vehicle: VehicleSummary) {
        if (vehicle) {
            if (vehicle.category === VehicleSize.small) {
                const a = this._smallFerry$.getValue().addVehicle(vehicle);
                if (a) {
                    this._smallFerry$.next(this._smallFerry$.getValue());
                }
            } else {
                const a = this._largeFerry$.getValue().addVehicle(vehicle);
                if (a) {
                    this._largeFerry$.next(this._largeFerry$.getValue());
                }
            }
        } else {
            throw new Error('Enter a valid vehicle');
        }
    }
}